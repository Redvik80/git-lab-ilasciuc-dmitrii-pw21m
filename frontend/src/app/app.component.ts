import { Component, } from '@angular/core';
import { PrimeNGConfig } from 'primeng-lts/api';
import { ruTranslations } from 'src/_utils/primeNgTranslations';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(
        private primeNgConfig: PrimeNGConfig
    ) {
        this.primeNgConfig.setTranslation(ruTranslations);
    }
}