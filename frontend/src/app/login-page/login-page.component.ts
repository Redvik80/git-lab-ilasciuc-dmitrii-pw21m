import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

    login: string = "001";
    password: string = "001";

    constructor(
        private http: HttpClient,
        private router: Router
    ) { }

    ngOnInit() { }

    getToken() {
        this.http.post("/login", { login: this.login, password: this.password }).subscribe((token: string) => {
            localStorage.setItem("token", token);
            this.router.navigateByUrl("/");
        });
    }
}
