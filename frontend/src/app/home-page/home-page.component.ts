import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, SelectItem } from 'primeng-lts/api';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

    tasks: Task[] = [];
    selectedTask: Task;

    listModeVariants: SelectItem<HomePageComponent["listMode"]>[] = [
        { label: "Все", value: "all" },
        { label: "По дате", value: "byDate" }
    ];
    listMode: "all" | "byDate" = "all";

    selectedDate = new Date;
    yearNow = (new Date).getFullYear();

    dateNow = new Date;
    dateNowStr = this.dateToIsoStr(this.dateNow);


    constructor(
        private http: HttpClient,
        private confirmS: ConfirmationService,
        private router: Router
    ) { }

    ngOnInit() {
        this.getTasksByListMode();
    }

    getTasksByListMode() {
        if (this.listMode === "all") {
            this.http.get("/tasks", { params: { mode: "all" } }).subscribe((data: Task[]) => {
                this.tasks = data;
            });
        }
        else {
            this.http.get("/tasks", { params: { mode: "byDate", date: this.dateToIsoStr(this.selectedDate) } }).subscribe((data: Task[]) => {
                this.tasks = data;
            });
        }
    }

    addNewTask() {
        this.http.post("/tasks", {}).subscribe((task: Task) => {
            if (this.listMode === "all" || this.dateToIsoStr(this.selectedDate) === task.date) this.tasks.push(task);
            this.selectTaskForChanges(task);
        });
    }

    selectTaskForChanges(task: Task) {
        this.selectedTask = JSON.parse(JSON.stringify(task));
        this.selectedTask.dateObj = this.isoStrToDate(this.selectedTask.date);
    }

    changeTask() {
        this.http.put("/tasks", this.selectedTask).subscribe(() => {
            delete this.selectedTask.dateObj;
            this.tasks = this.tasks.filter((item) => item.id !== this.selectedTask.id);
            if (this.listMode === "all" || this.dateToIsoStr(this.selectedDate) === this.selectedTask.date) this.tasks.push(this.selectedTask);
            this.selectedTask = null;
        });
    }

    deleteTask(taskId: number) {
        this.confirmS.confirm({
            message: "Вы действитеьно хотите удалить эту заметку?",
            accept: () => {
                this.http.delete("/tasks", { params: { id: taskId + "" } }).subscribe(() => {
                    this.tasks = this.tasks.filter((item) => item.id != taskId);
                });
            }
        });
    }

    dateToIsoStr(date: Date) {
        if (!date) return null;
        return `${date.getFullYear()}-${date.getMonth() > 8 ? "" : "0"}${date.getMonth() + 1}-${date.getDate() > 9 ? "" : "0"}${date.getDate()}`;
    }

    isoStrToDate(date: string) {
        if (!date) return null;
        return new Date(+date.substr(0, 4), +date.substr(5, 2) - 1, +date.substr(8, 2));
    }

    logout() {
        localStorage.removeItem("token");
        this.router.navigateByUrl("/login");
    }
}


class Task {
    id: number;
    text: string;
    date: string;
    userId: number;

    dateObj?: Date;
}