import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(
        private http: HttpClient,
        private router: Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return new Promise((resolve) => {
            this.http.get("/check-token").subscribe((resp: boolean) => {
                resolve(resp);
                if (!resp) this.router.navigateByUrl("/login");
            }, () => {
                resolve(false);
                this.router.navigateByUrl("/login");
            });
        })
    }
}
