import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';



@Injectable()
export class AppInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let headers = {}
        let url = req.url;
        if (req.url.startsWith("/")) {
            if (!req.headers.get("Content-Type") && !(req.body instanceof FormData)) {  // в node нет FormData
                headers["Content-Type"] = "application/json";  // FormData - передаём файлы
            }
            headers["Authorization"] = localStorage.getItem("token") || "";
            url = environment.backendAddress + req.url;
        }
        return next.handle(req.clone({ setHeaders: headers, url }));
    }
}
