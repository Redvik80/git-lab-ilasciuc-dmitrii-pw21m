package mainpackage.controllers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import mainpackage.models.LoginData;
import mainpackage.services.DbService;

@CrossOrigin(origins = "*")
@Controller
@ResponseBody
public class AuthController {

    private DbService dbS;
    private ObjectMapper json;

    @Autowired
    public AuthController(DbService dbS) {
        this.dbS = dbS;
        this.json = new ObjectMapper();
        this.json.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
    }

    @RequestMapping("/check-token")
    @GetMapping()
    public String checkToken(@RequestHeader("Authorization") String token) throws SQLException {
        if (token.length() == 0) {
            return "false";
        }
        PreparedStatement findDbReq = this.dbS.connection.prepareStatement("SELECT * FROM users WHERE token = ?;");
        findDbReq.setString(1, token);
        ResultSet dbResp = findDbReq.executeQuery();
        if (dbResp.next()) {
            return "true";
        } else {
            return "false";
        }
    }

    @RequestMapping("/login")
    @PostMapping()
    public String login(@RequestBody LoginData loginData) throws SQLException {
        PreparedStatement findDbReq = this.dbS.connection
                .prepareStatement("SELECT * FROM users WHERE \"login\" = ? AND \"password\" = ?;");
        findDbReq.setString(1, loginData.login);
        findDbReq.setString(2, loginData.password);
        ResultSet dbResp = findDbReq.executeQuery();
        if (dbResp.next()) {
            Integer userId = dbResp.getInt("id");
            String newToken = userId.toString() + "." + UUID.randomUUID().toString();
            PreparedStatement updateDbReq = this.dbS.connection
                    .prepareStatement("UPDATE users SET token = ? WHERE id = ?;");
            updateDbReq.setString(1, newToken);
            updateDbReq.setInt(2, userId);
            updateDbReq.executeUpdate();
            return '"' + newToken + '"';
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

}