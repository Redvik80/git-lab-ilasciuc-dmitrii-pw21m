package mainpackage.controllers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import mainpackage.models.Task;
import mainpackage.services.DbService;

@CrossOrigin(origins = "*")
@Controller
@RequestMapping("/tasks")
@ResponseBody
public class TasksController {

    private DbService dbS;
    private ObjectMapper json;

    @Autowired
    public TasksController(DbService dbS) {
        this.dbS = dbS;
        this.json = new ObjectMapper();
        this.json.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
    }

    @GetMapping()
    public String getTasks(@RequestParam String mode, @RequestParam(required = false) String date,
            @RequestHeader("Authorization") String token) throws SQLException, JsonProcessingException {

        Integer userId = checkToken(token);

        if (mode.equals("all")) {
            PreparedStatement dbReq = this.dbS.connection
                    .prepareStatement("SELECT * FROM tasks WHERE \"userId\" = ? ORDER BY date DESC, text;");
            dbReq.setInt(1, userId);
            ResultSet dbResp = dbReq.executeQuery();
            ArrayList<Task> tasks = new ArrayList<Task>();
            while (dbResp.next()) {
                tasks.add(new Task(dbResp));
            }
            return this.json.writeValueAsString(tasks);
        } else if (mode.equals("byDate") && date != null) {
            PreparedStatement dbReq = this.dbS.connection.prepareStatement(
                    "SELECT * FROM tasks WHERE date = ?::DATE AND \"userId\" = ? ORDER BY date DESC, text;");
            dbReq.setString(1, date);
            dbReq.setInt(2, userId);
            ResultSet dbResp = dbReq.executeQuery();
            ArrayList<Task> tasks = new ArrayList<Task>();
            while (dbResp.next()) {
                tasks.add(new Task(dbResp));
            }
            return this.json.writeValueAsString(tasks);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping()
    public String addTask(@RequestHeader("Authorization") String token) throws SQLException, JsonProcessingException {

        Integer userId = checkToken(token);

        Task newTask = new Task();
        newTask.text = "Новое напоминание";
        newTask.date = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
        PreparedStatement dbReq = this.dbS.connection.prepareStatement(
                "INSERT INTO tasks VALUES (DEFAULT, ?, ?::DATE, ?);", Statement.RETURN_GENERATED_KEYS);
        dbReq.setString(1, newTask.text);
        dbReq.setString(2, newTask.date);
        dbReq.setInt(3, userId);
        dbReq.executeUpdate();
        ResultSet dbResp = dbReq.getGeneratedKeys();
        dbResp.next();
        newTask.id = dbResp.getInt(1);
        return this.json.writeValueAsString(newTask);
    }

    @PutMapping()
    public String changeTask(@RequestBody Task task, @RequestHeader("Authorization") String token) throws SQLException {

        Integer userId = checkToken(token);

        PreparedStatement dbReq = this.dbS.connection
                .prepareStatement("UPDATE tasks SET text = ?, date = ?::DATE WHERE id = ? AND \"userId\" = ?;");
        dbReq.setString(1, task.text);
        dbReq.setString(2, task.date);
        dbReq.setInt(3, task.id);
        dbReq.setInt(4, userId);
        dbReq.executeUpdate();
        return "";
    }

    @DeleteMapping()
    public String deleteTask(@RequestParam String id, @RequestHeader("Authorization") String token)
            throws SQLException {

        Integer userId = checkToken(token);

        PreparedStatement dbReq = this.dbS.connection
                .prepareStatement("DELETE FROM tasks WHERE id = ? AND \"userId\" = ?;");
        dbReq.setInt(1, Integer.parseInt(id));
        dbReq.setInt(2, userId);
        dbReq.executeUpdate();
        return "";
    }

    private Integer checkToken(String token) throws SQLException {
        PreparedStatement findDbReq = this.dbS.connection.prepareStatement("SELECT * FROM users WHERE token = ?;");
        findDbReq.setString(1, token);
        ResultSet dbResp = findDbReq.executeQuery();
        if (dbResp.next()) {
            return dbResp.getInt("id");
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }
}
