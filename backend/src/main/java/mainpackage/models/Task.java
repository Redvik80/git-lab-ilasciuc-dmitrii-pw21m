package mainpackage.models;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Task {
    public Integer id;
    public String text;
    public String date;
    public Integer userId;

    public Task(ResultSet dbData) throws SQLException {
        this.id = dbData.getInt("id");
        this.text = dbData.getString("text");
        this.date = dbData.getString("date");
        this.userId = dbData.getInt("userId");
    }

    public Task() {
    }
}
