package mainpackage.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan("mainpackage")
@EnableWebMvc
public class SpringConfig implements WebMvcConfigurer {

    // private final ApplicationContext applicationContext;

    // @Autowired
    // public SpringConfig(ApplicationContext applicationContext) {
    // this.applicationContext = applicationContext;
    // }
}