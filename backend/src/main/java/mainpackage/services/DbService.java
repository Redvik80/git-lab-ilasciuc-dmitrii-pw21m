package mainpackage.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.stereotype.Component;

@Component
public class DbService {

    public Connection connection;

    public DbService() {
        try {
            Class.forName("org.postgresql.Driver");
            try {
                this.connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/task-book-java",
                        "postgres", "123");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
