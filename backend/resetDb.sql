DROP TABLE IF EXISTS tasks;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    "login" TEXT,
    "password" TEXT,
    token TEXT
);

CREATE TABLE tasks (
    id SERIAL PRIMARY KEY,
    "text" TEXT,
    "date" DATE,
    "userId" INT REFERENCES users
);

INSERT INTO users VALUES (DEFAULT, '001', '001', NULL);